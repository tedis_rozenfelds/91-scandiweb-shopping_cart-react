Tutorial: https://www.freakyjolly.com/react-table-with-select-unselect-rows-using-checkboxes-example/

ToDoList:
1. Crate a MYSQL database with table products, develop a structure for the products table according to task requirements

2. Crate a PHP REST API to accept GET and PUT requests where GET will return all the products and PUT will add a new product. The PHP script will either read or put data within the MYSQL database table products. Response must be return in the format of JSON and headers like 
Access-Control-Allow-Methods
Access-Control-Allow-Headers
Access-Control-Allow-Origin 
must be added for the API to work.

3. Crate a REACT client app, which will consume the PHP REST api https://www.positronx.io/how-to-use-fetch-api-to-get-data-in-react-with-rest-api/
4. Crate a simplistic UI using react (Bootstrap is already built-in to react)

Primarily focus on creating the GET products, after that is done, add the PUT method