import React from "react";
import { Link } from 'react-router-dom';
import './index.css';
const API = [];
const url = "https://semblable-performan.000webhostapp.com/api/index.php";


class SelectTableComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      List: API,
      MasterChecked: false,
      SelectedList: [],
    };
  }

  componentDidMount() {
    // Simple GET request using fetch
    fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ List: data }));
    }

  delete() {
    // Simple DELETE request with fetch
    fetch(url, { 
      method: 'DELETE' ,
      body: JSON.stringify(this.state.SelectedList)
      })
        .then(() => this.setState({ status: 'Delete successful' })
        )};

  // Update List Item's state and Master Checkbox State
  onItemCheck(e, item) {
    let tempList = this.state.List;
    tempList.map((product) => {
      if (product.sku === item.sku) {
        product.selected = e.target.checked;
      }
      return product;
    });

    // Update State
    this.setState({
/*       MasterChecked: totalItems === totalCheckedItems,
 */      List: tempList,
      SelectedList: this.state.List.filter((e) => e.selected),
    });
  }

  // Event to get selected rows(Optional)
  getSelectedRows() {
    this.setState({
      SelectedList: this.state.List.filter((e) => e.selected),
    });
  }

  render() {
    return (
      <div>
        
          <div >
            <table className="table">
              <tbody>
                {this.state.List.map((product) => (
                  <tr key={product.sku} className={product.selected ? "selected" : ""}>
                    <th scope="row">
                      <input
                        type="checkbox"
                        checked={product.selected}
                        className=".delete-checkbox"
                        id="rowcheck{product.sku}"
                        onChange={(e) => this.onItemCheck(e, product)}
                      />
                    </th>
                    <td>{product.sku}</td>
                    <td>{product.name}</td>
                    <td>{product.Price}</td>
                    <td>{product.size}</td>
                    <td>{product.weight}</td>
                    <td>{product.dimensions}</td>
                  </tr>
                ))}
              </tbody>
            </table>

            <button
              id="#delete-product-bin"
              style={{
                position: 'absolute',
                right: 5,
                top: 5,
                }} 
              onClick={() => {this.delete()}} >
              MASS DELETE
            </button>

            <nav>
            <Link               
            style={{
                position: 'absolute',
                right: 150,
                top: 5,
                }} 
            to="/add-product">ADD</Link> |{" "}
            </nav>
          </div>
             </div>
    );
  }
}

export default SelectTableComponent;