import "./App.css";
import { useState } from "react";
import { Link } from 'react-router-dom';


function ProductForm() {
  const [sku, setsku] = useState("");
  const [name, setname] = useState("");
  const [price, setprice] = useState("");
  const [size, setsize] = useState("");
  const [weight, setweight] = useState("");
  const [dimensions, setdimensions] = useState("");
  const [message, setMessage] = useState("");

  let handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let res = await fetch("https://semblable-performan.000webhostapp.com/api/index.php", {
        method: "POST",
        body: JSON.stringify({
          sku: sku,
          name: name,
          price: price,
          size: size,
          weight: weight,
          dimensions: dimensions,
        }),
      });
/*       let resJson = await res.json(); */
      if (res.status === 200) {
        setsku("");
        setname("");
        setprice("");
        setsize("");
        setweight("");
        setdimensions("");
        setMessage("User created successfully");
      } else {
        setMessage("Some error occured");
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="App">
      <form id = "#product_form" onSubmit={handleSubmit}>
        <input
          id="#sku"
          type="text"
          value={sku}
          placeholder="sku"
          onChange={(e) => setsku(e.target.value)}
        />
        <input
          id="#name"
          type="text"
          value={name}
          placeholder="name"
          onChange={(e) => setname(e.target.value)}
        />
        <input
          id="#price"
          type="text"
          value={price}
          placeholder="price ($)"
          onChange={(e) => setprice(e.target.value)}
        />
        <input
          id="#size"
          type="text"
          value={size}
          placeholder="size"
          onChange={(e) => setsize(e.target.value)}
        />
        <input
          id="#weight"
          type="text"
          value={weight}
          placeholder="weight"
          onChange={(e) => setweight(e.target.value)}
        />
        <input
          id="#dimensions"
          type="text"
          value={dimensions}
          placeholder="dimensions"
          onChange={(e) => setdimensions(e.target.value)}
        />

        <button type="submit">Save</button>
        <Link to="/">Cancel</Link> |{" "}

        <div className="message">{message ? <p>{message}</p> : null}</div>
      </form>
    </div>
  );
}

export default ProductForm;



